function costWithSales(sum, items, promo = '') {
  promo = promo.toUpperCase();
  if (promo === 'ДАРИМ300') {
    sum = sum > 300 ? sum - 300 : 0;
  }
  if (items >=10) {
    sum *= 0.95;
  }
  if (sum > 50000) {
    sum -= (sum - 50000) * 0.2;
  }
  if (promo === 'СКИДКА15' && sum >= 20000) {
    sum *= 0.85;
  }
  return sum;
}

console.log(costWithSales(30000, 15, 'дарим300')); // 28 215
console.log(costWithSales(30000, 15, 'СКИДКА15')); // 24 225
console.log(costWithSales(20000, 9, 'СКИДКА15')); // 17 000
console.log(costWithSales(55000, 10, 'скидка15')); // 44 030
console.log(costWithSales(15000, 10, 'СКИДКА15')); // 14 250
console.log(costWithSales(200, 7)); // 200
console.log(costWithSales(200, 7, 'ДАРИМ300')); // 0
console.log(costWithSales(300, 14, 'ДАРИМ300')); // 0
