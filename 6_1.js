'use strict';

let objects = [
  {name: 'Василий', surname: 'Иванов'},
  {name: 'Иван', surname: 'Иванов'},
  {name: 'Иван', surname: 'Иванов'},
];

function filterObjects(objects, propertyName, value) {
  const result = [];
  for (const object of objects) {
    const {[propertyName]: propsValue} = object;
    if (propsValue === value) {
      result.push(object);
    }
  }
  return result;
}

let result1 = filterObjects(objects, 'name', 'Иван'); // [{ name: 'Иван', surname: 'Иванов' }]
let result2 = filterObjects(objects, 'surname', 'Иванов');

console.log(result1);
console.log(result2);