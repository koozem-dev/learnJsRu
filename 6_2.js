'use strict';

let objects = [
  ['Василий', 'Иванов'],
  ['Иван', 'Иванов'],
  ['Иван', 'Иванов'],
];

function filterObjects(objects, index, value) {
  const result = [];
  for (const arr of objects) {
    if (arr[index] === value) {
      result.push(arr);
    }
  }
  return result;
}

let result1 = filterObjects(objects, 0, 'Иван'); // [{ name: 'Иван', surname: 'Иванов' }]
let result2 = filterObjects(objects, 1, 'Иванов');

console.log(result1);
console.log(result2);

// а есть элегантное встроенное решение
function filterObjects2(objects, index, value) {
  return objects.filter(object => object[index] === value);
}
