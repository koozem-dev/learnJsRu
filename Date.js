'use strict';

// Создайте дату 20 февраля 2012 года, 3 часа 12 минут. Временная зона – местная.
let now = new Date(2012, 1, 20, 3, 12);
console.log(now);

// Покажите день недели
function getWeekDay(date) {
  switch (date.getDay()) {
    case 0 :
      return 'ВС';
    case 1 :
      return 'ПН';
    case 2 :
      return 'ВТ';
    case 3 :
      return 'СР';
    case 4 :
      return 'ЧТ';
    case 5 :
      return 'ПТ';
    case 6 :
      return 'СБ';
  }
}

let date = new Date(2012, 0, 3);  // 3 января 2012 года
// console.log(getWeekDay(date));        // нужно вывести "ВТ"
console.log(getWeekDay(date));        // нужно вывести "ВТ"
console.log(date.getDay());

function getWeekDay2(date) {
  let days = ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'];

  return days[date.getDay()];
}

// День недели в европейской нумерации
function getLocalDay(date) {
  let day = date.getDay();

  if (day === 0) {
    day = 7;
  }
  return day;
}

let date1 = new Date(2012, 0, 3);  // 3 января 2012 года
console.log(getLocalDay(date1)); // вторник, нужно показать 2

const msInSecond = 1000;
const msInMinutes = 60 * msInSecond;
const msInHours = 60 * msInMinutes;
const msInDay = 24 * 3600 * 1000;
const second = 1;
const secondsInMinute = 60 * second;
const secondsInHour = 60 * secondsInMinute;

// Какой день месяца был много дней назад?
function getDateAgo(date, days) {
  return new Date(date - (days * msInDay)).getDate();
}

let date3 = new Date(2015, 0, 2);

console.log(getDateAgo(date3, 1)); // 1, (1 Jan 2015)
console.log(getDateAgo(date3, 2)); // 31, (31 Dec 2014)
console.log(getDateAgo(date3, 365)); // 2, (2 Jan 2014)

// Последнее число месяца?
function getLastDayOfMonth(year, month) {
  return new Date(new Date(year, month + 1) - msInDay).getDate();
}

console.log(getLastDayOfMonth(2012, 0)); // 31
console.log(getLastDayOfMonth(2012, 1)); // 29
console.log(getLastDayOfMonth(2013, 1)); // 28

function getLastDayOfMonth2(year, month) {
  return new Date(year, month + 1, 0).getDate();
}

// Сколько сегодня прошло секунд?
function getSecondsToday() {
  let now = new Date();
  let today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

  let diff = now - today;
  return Math.round(diff / msInSecond);
}

console.log(getSecondsToday());

function getSecondsToday2() {
  let d = new Date();
  return d.getHours() * secondsInHour + d.getMinutes() * secondsInMinute +
      d.getSeconds();
}

// Сколько секунд осталось до завтра?
function getSecondsToTomorrow() {
  let now = new Date();

  let tomorrow = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1);
  return Math.round((tomorrow - now) / msInSecond);
}

function getSecondsToTomorrow2() {
  let now = new Date();
  let hour = now.getHours();
  let minutes = now.getMinutes();
  let seconds = now.getSeconds();
  let totalSecondsToday = (hour * 60 + minutes) * 60 + seconds;
  let totalSecondsInADay = 86400;

  return totalSecondsInADay - totalSecondsToday;
}

console.log(getSecondsToTomorrow());
console.log(getSecondsToTomorrow2());

// Форматирование относительной даты
// Если спустя date прошло менее 1 секунды, вывести "прямо сейчас".
// В противном случае, если с date прошло меньше 1 минуты, вывести "n сек. назад".
// В противном случае, если меньше часа, вывести "m мин. назад".
// В противном случае, полная дата в формате "DD.MM.YY HH:mm". А именно:
// "день.месяц.год часы:минуты", всё в виде двух цифр, т.е. 31.12.16 10:00.
function formatDate(date) {
  const diff = new Date() - date;

  if (diff < msInSecond) {
    return 'прямо сейчас';
  }

  if (diff < msInMinutes) {
    return `${Math.floor(diff / msInSecond)} сек. назад`;
  }

  if (diff < msInHours) {
    return `${Math.floor(diff / msInMinutes)} мин. назад`;
  }

  let d = date;
  d = [
    '0' + d.getDate(),
    '0' + (d.getMonth() + 1),
    '' + d.getFullYear(),
    '0' + d.getHours(),
    '0' + d.getMinutes(),
  ].map(component => component.slice(-2)); // взять последние 2 цифры из каждой компоненты

  // соединить компоненты в дату
  return d.slice(0, 3).join('.') + ' ' + d.slice(3).join(':');
}

console.log(formatDate(new Date(new Date - 1))); // "прямо сейчас"
console.log(formatDate(new Date(new Date - 30 * 1000))); // "30 сек. назад"
console.log(formatDate(new Date(new Date - 5 * 60 * 1000))); // "5 мин. назад"
// вчерашняя дата вида 31.12.16 20:00
console.log(formatDate(new Date(new Date - 86400 * 1000)));

// преобразование даты в формат DD.MM.YY HH:mm
function formatDate2(date) {
  let optionsOfDate = {
    year: '2-digit',
    month: '2-digit',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
  };

  return date.toLocaleDateString('ru-RU', optionsOfDate).split(',').join('');
}

console.log(formatDate2(new Date(new Date - 86400 * 1000)));