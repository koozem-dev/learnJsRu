'use strict';

// Фильтрация уникальных элементов массива
function unique(arr) {
  return Array.from(new Set(arr));
}

let values = [
  'Hare', 'Krishna', 'Hare', 'Krishna',
  'Krishna', 'Krishna', 'Hare', 'Hare', ':-O',
];

console.log(unique(values)); // Hare,Krishna,:-O

// Отфильтруйте анаграммы
// отсортированные строки как ключи в коллекции Map, для того чтобы сопоставить
// каждому ключу только одно значение
function aclean(arr) {
  let map = new Map();
  arr.forEach(item => {
    map.set(item.toLowerCase().split('').sort().join(''), item);
  });
  return Array.from(map.values());
}

let arr = ['nap', 'teachers', 'cheaters', 'PAN', 'ear', 'era', 'hectares'];

console.log(aclean(arr)); // "nap,teachers,ear" or "PAN,cheaters,era"

// Перебираемые ключи
let map1 = new Map();

map1.set("name", "John");

let keys = Array.from(map1.keys());

keys.push("more");
