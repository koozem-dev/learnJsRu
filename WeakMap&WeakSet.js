'use strict';

// Хранение отметок "не прочитано"
// Когда сообщение удаляется из messages, оно также исчезает из структуры данных
let messages = [
  {text: 'Hello', from: 'John'},
  {text: 'How goes?', from: 'John'},
  {text: 'See you soon', from: 'Alice'},
];

let readingMessages = new WeakSet();
readingMessages.add(messages[0]);
readingMessages.add(messages[1]);
readingMessages.add(messages[0]);

console.log('read msg 0: ' + readingMessages.has(messages[0]));

messages.shift();

console.log(messages);
console.log('read msg 1: ' + readingMessages.has(messages[1]));

// Хранение времени прочтения
let messages2 = [
  { text: "Hello", from: "John" },
  { text: "How goes?", from: "John" },
  { text: "See you soon", from: "Alice" }
];

let readingMessages2 = new WeakMap();
readingMessages2.set(messages2[0], new Date());
readingMessages2.set(messages2[1], new Date());

console.log(readingMessages2.get(messages2[0]));
console.log(readingMessages2.get(messages2[1]));

messages2.shift();
console.log(readingMessages2.get(messages2[1]));