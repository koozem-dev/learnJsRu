// основные функции массива
let styles = [
  'Джаз',
  'Блюз',
];

styles.push('Рок-н-ролл');
styles[Math.floor((styles.length - 1) / 2)] = 'Классика';

console.log(styles.shift());
styles.unshift('Рэп', 'Регги');

// контекст в массиве
let arr = ['a', 'b'];

arr.push(function() {
  console.log(this);
});

arr[2](); // ['a', 'b', ƒ ()]

//Сумма введённых чисел
function sumInput1() {
  let x = 0;
  const arrInputs = [];
  let sum = 0;
  while (true) {
    x = prompt('Введите число для суммирования', 0);
    if (x === '' || x === null || !isFinite(+x)) {
      break;
    }
    arrInputs.push(+x);
  }
  for (let number of arrInputs) {
    sum += number;
  }
  return sum;
}

function sumInput2() {
  let x = 0;
  const arrInputs = [];
  let sum = 0;
  while (true) {
    x = prompt('Введите число для суммирования', 0);
    if (x === '' || x === null || !isFinite(+x)) {
      break;
    }
    arrInputs.push(+x);
    sum += sum;
  }
  return sum;
}

//Подмассив наибольшей суммы
// находит непрерывный подмассив в arr, сумма элементов в котором максимальна.
// возвращает эту сумму.
// Если все элементы отрицательные – ничего не берём(подмассив пустой) и сумма равна «0»
function getMaxSubSum(arr) {
  let maxSum = 0;
  let subSum = 0;
  for (const number of arr) {
    if (number < 0 && maxSum === 0) {
      continue;
    }
    subSum = Math.max((subSum + number), number);
    maxSum = Math.max(maxSum, subSum);
  }
  return maxSum;
}

getMaxSubSum([-1, 2, 3, -9]); // = 5
getMaxSubSum([2, -1, 2, 3, -9]); // = 6
getMaxSubSum([-1, 2, 3, -9, 11]); // = 11
getMaxSubSum([-2, -1, 1, 2]); // = 3
getMaxSubSum([100, -9, 2, -3, 5]); // = 100
getMaxSubSum([1, 2, 3]); // = 6 (берём все)
getMaxSubSum([-1]); // = 0
getMaxSubSum([]); // = 0

// решение поэлегантнее
function getMaxSubSum1(arr) {
  let maxSum = 0;
  let subSum = 0;
  for (const number of arr) {
    subSum = Math.max((subSum + number), 0);
    maxSum = Math.max(maxSum, subSum);
  }
  return maxSum;
}