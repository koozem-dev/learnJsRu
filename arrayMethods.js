'use strict';

// преобразует строки вида «my-short-string» в «myShortString»
function camelize(str) {
  let camelizeArr = str.split('-').map((item, index) => {
    if (index > 0) {
      return item[0].toUpperCase() + item.slice(1);
    }
    return item;
  });
  return camelizeArr.join('');
}

console.log(camelize('background-color') === 'backgroundColor');
console.log(camelize('list-style-image') === 'listStyleImage');
console.log(camelize('-webkit-transition') === 'WebkitTransition');

function camelize2(str) {
  return str.split('-').map(
      (word, index) => index === 0 ?
          word :
          word[0].toUpperCase() + word.slice(1),
  ).join('');
}

// Фильтрация по диапазону. исходный массив не изменяется
function filterRange(arr, a, b) {
  return arr.filter(item => (a <= item && item <= b));
}

const arr4 = [5, 3, 4, 8];
let filtered = filterRange(arr4, 1, 4);

console.log(filtered); // 3,1 (совпадающие значения)
console.log(arr4); // 5,3,8,1 (без изменений)

// Фильтрация по диапазону "на месте"
function filterRangeInPlace(arr, a, b) {
  arr.forEach((item, index) => {
    if (!(a <= item && item <= b)) {
      arr.splice(index, 1);
    }
  });
}

let arr = [5, 3, 8, 1];
filterRangeInPlace(arr, 1, 4);
console.log(arr);

// Сортировать в порядке по убыванию
let arr1 = [5, 2, 1, -10, 8];

arr1.sort((a, b) => b - a);

console.log(arr1); // 8, 5, 2, 1, -10

//Скопировать и отсортировать массив
function copySorted(arr) {
  return arr.slice().sort();
}

let arr3 = ['HTML', 'JavaScript', 'CSS'];
let sorted = copySorted(arr3);

console.log(sorted); // CSS, HTML, JavaScript
console.log(arr3); // HTML, JavaScript, CSS (без изменений)

// расширяемый калькулятор
function Calculator() {
  this.operations = {
    '+': (a, b) => a + b,
    '-': (a, b) => a - b,
  };

  this.calculate = function(str) {
    if (typeof str !== 'string') {
      return;
    }

    this.operand = str.split(' ');
    let a = +this.operand[0],
        op = this.operand[1],
        b = +this.operand[2];
    if (!this.operations[op] || isNaN(a) || isNaN(b)) {
      return NaN;
    }
    return this.operations[op](a, b);
  };

  this.addMethod = function(operand, callback) {
    this.operations[operand] = callback;
  };
}

let calc = new Calculator;

console.log(calc.calculate('3 + 7')); // 10

let powerCalc = new Calculator;

powerCalc.addMethod('*', (a, b) => a * b);
powerCalc.addMethod('/', (a, b) => a / b);
powerCalc.addMethod('**', (a, b) => a ** b);

let result = powerCalc.calculate('2 ** 3');
console.log(result); // 8

// Трансформировать в массив имён
let vasya1 = {name: 'Вася', age: 25};
let petya1 = {name: 'Петя', age: 30};
let masha1 = {name: 'Маша', age: 28};

let users1 = [vasya1, petya1, masha1];

let names = users1.map(user => user.name);
console.log(names); // Вася, Петя, Маша

// Трансформировать в объекты
let vasya = {name: 'Вася', surname: 'Пупкин', id: 1};
let petya = {name: 'Петя', surname: 'Иванов', id: 2};
let masha = {name: 'Маша', surname: 'Петрова', id: 3};

let users = [vasya, petya, masha];

let usersMapped = users.map(user => ({
  fullName: `${user.name} ${user.surname}`,
  id: user.id,
}));

/*
usersMapped = [
  { fullName: "Вася Пупкин", id: 1 },
  { fullName: "Петя Иванов", id: 2 },
  { fullName: "Маша Петрова", id: 3 }
]
*/

console.log(usersMapped[0].id); // 1
console.log(usersMapped[0].fullName); // Вася Пупкин

// Отсортировать пользователей по возрасту
function sortByAge(arrayOfObjects) {
  arrayOfObjects.sort((a, b) => a['age'] - b['age']);
}

let vasya2 = {name: 'Вася', age: 25};
let petya2 = {name: 'Петя', age: 30};
let masha2 = {name: 'Маша', age: 28};

let arrr = [vasya2, petya2, masha2];

sortByAge(arrr);

// теперь: [vasya, masha, petya]
console.log(arrr[0].name); // Вася
console.log(arrr[1].name); // Маша
console.log(arrr[2].name); // Петя

// Перемешайте массив
function shuffle(array) {
  for (let i = array.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1)); // случайный индекс от 0 до i
    // поменять элементы местами
    // используем для этого синтаксис "деструктурирующее присваивание"
    [array[i], array[j]] = [array[j], array[i]];
  }
}

let array = [1, 2, 3];

shuffle(array);
console.log(array); // arr = [3, 2, 1]

shuffle(array);
console.log(array); // arr = [2, 1, 3]

shuffle(array);
console.log(array); // arr = [3, 1, 2]

// Получить средний возраст
function getAverageAge(users) {
  return users.reduce((sum, user) => sum + user.age, 0) / users.length;
}

let vasyaLast = {name: 'Вася', age: 25};
let petyaLast = {name: 'Петя', age: 30};
let mashaLast = {name: 'Маша', age: 29};

let arrLast = [vasyaLast, petyaLast, mashaLast];

console.log(getAverageAge(arrLast)); // (25 + 30 + 29) / 3 = 28

// Оставить уникальные элементы массива
function unique(arr) {
  return arr.reduce((newArr, item) => {
    if (!newArr.includes(item)) {
      newArr.push(item);
    }
    return newArr;
  }, []);
}

let strings = [
  'кришна', 'кришна', 'харе', 'харе',
  'харе', 'харе', 'кришна', 'кришна', ':-O',
];

console.log(unique(strings)); // кришна, харе, :-O