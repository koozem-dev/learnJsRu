'use strict';

function ask(question, yes, no) {
  (1) ? yes() : no();
}

ask(
    'Вы согласны?',
    () => console.log('yes'),
    () => console.log('no'),
);