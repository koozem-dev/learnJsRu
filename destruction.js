'use strict';

// Деструктурирующее присваивание
let user = {name: 'John', years: 30};

// ваш код должен быть с левой стороны:
let {name, years: age, isAdmin = false} = user;

console.log(name); // John
console.log(age); // 30
console.log(isAdmin); // false

// Максимальная зарплата
let salaries = {
  'John': 100,
  'Mary': 250,
  'Pete': 300,
};

function topSalary(salaries) {
  let maxCost = 0;
  let maxName = null;

  for (const [name, cost] of Object.entries(salaries)) {
    if (maxCost < cost) {
      maxCost = cost;
      maxName = name;
    }
  }
  return maxName;
}

console.log(topSalary(salaries));