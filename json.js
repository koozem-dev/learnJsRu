'use strict';

let user = {
  name: 'John',
  age: 30,
};

console.log(JSON.stringify(user));

// Преобразуйте объект в JSON, а затем обратно в обычный объект
let user1 = {
  name: 'Василий Иванович',
  age: 35,
};

let json = JSON.stringify(user1);
console.log(json);

let obj = JSON.parse(json);
console.log(obj);

// Исключить обратные ссылки
let room = {
  number: 23,
};

let meetup = {
  title: 'Совещание',
  occupiedBy: [{name: 'Иванов'}, {name: 'Петров'}],
  place: room,
};

// цикличные ссылки
room.occupiedBy = meetup;
meetup.self = meetup;

console.log(JSON.stringify(meetup, function replacer(key, value) {
  if (key !== '' && value === meetup) {
    return undefined;
  }
  console.log(`key = ${key}, value = ${value}`);
  return value;
}));

/* в результате должно быть:
{
  "title":"Совещание",
  "occupiedBy":[{"name":"Иванов"},{"name":"Петров"}],
  "place":{"number":23}
}
*/