let obj = {};

function A() {
  return obj;
}

function B() {
  return obj;
}

console.log(new A() === new B()); // true

// калькулятор (сумма, произведение)
function Calculator() {
  this.a = 2;
  this.b = 6;

  this.read = function() {
    // здесь должен быть ввод переменных
    console.log('a = ', this.a);
    console.log('b = ', this.b);
  };
  this.sum = function() {
    return this.a + this.b;
  };
  this.mul = function() {
    return this.a * this.b;
  };

}

let calculator = new Calculator();
calculator.read();

console.log('Sum=' + calculator.sum());
console.log('Mul=' + calculator.mul());

// Создаём Accumulator
function Accumulator(startValue) {
  this.value = startValue;
  this.read = function() {
    // здесь должен быть ввод переменных
    this.add = 5;
    this.value += this.add;
  };
}

let accumulator = new Accumulator(1); // начальное значение 1

accumulator.read(); // прибавит ввод prompt к текущему значению
accumulator.read(); // прибавит ввод prompt к текущему значению

console.log(accumulator.value); // выведет сумму этих значений