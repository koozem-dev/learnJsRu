'use strict';

// проверка существования свойства in obj
let obj = {
  test: undefined,
};

console.log(obj.test); // undefined
console.log('test' in obj); // true
console.log('bloblo' in obj); // false

// добавление, удаление свойств в объект
let user = {};

user.name = 'John';
user.surname = 'Smith';
console.log(user);

user.name = 'Pete';
console.log(user.name);

delete user.name;
console.log(user.name);

// проверка на пустоту
let schedule = {};

console.log(isEmpty(schedule)); // true

schedule['8:30'] = 'get up';

console.log(isEmpty1(schedule)); // false

function isEmpty1(obj) {
  // 1 решение
  for (let key in obj) {
    return false;
  }
  return true;
}

function isEmpty2(obj) {
  // 2 решение (есть проблема, если значение первого ключа undefined)
  let keys = Object.keys(obj);
  return keys[0] === undefined;
}

function isEmpty3(obj) {
  // 3 решение
  let keys = Object.keys(obj);
  return keys.length === 0;
}

// сумма свойств объекта
let salaries = {
  John: 100,
  Ann: 160,
  Pete: 130,
};

function getSumSalaries(obj) {
  let sum = 0;
  for (let sale in obj) {
    sum += obj[sale];
  }
  return sum;
}

// умножаем все числовые свойства на 2
// до вызова функции
let menu = {
  width: 200,
  height: 300,
  title: 'My menu',
};

multiplyNumeric(menu);

function multiplyNumeric(obj) {
  for (let property in obj) {
    if (typeof obj[property] === 'number') {
      obj[property] *= 2;
    }
  }
}

console.log(menu);