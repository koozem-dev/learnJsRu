'use strict';

// Вычислить сумму чисел до данного
// 1. С использованием цикла. (не самый медленный вариант)
function sumTo(n) {
  let sum = 0;
  for (let i = n; i > 0; i--) {
    sum += i;
  }
  return sum;
}

console.log(sumTo(100)); // 5050

// 2. Через рекурсию, т.к. sumTo(n) = n + sumTo(n-1) for n > 1. (самый медленный)
function sumTo2(n) {
  if (n === 1) {
    return n;
  }
  return n + sumTo2(n - 1);
}

console.log(sumTo2(100)); // 5050

// 3. С использованием формулы арифметической прогрессии. (самый быстрый)
function sumTo3(n) {
  return n * (n + 1) / 2;
}

console.log(sumTo3(100)); // 5050

// Вычислить факториал
function factorial(n) {
  if (n === 1) {
    return n;
  }
  return n * factorial(n - 1);
}

console.log(factorial(5)); // 120

// Числа Фибоначчи.
// функция возвращает n-е число Фибоначчи
function fib(n) {
  let prev = 1;
  let current = 1;
  let next = 0;
  for (let i = 3; i <= n; i++) {
    next = prev + current;
    [prev, current] = [current, next];
  }
  return next;
}

console.log(fib(3)); // 2
console.log(fib(7)); // 13
console.log(fib(77)); // 5527939700884757

// слишком не оптимально
function fib2(n) {
  return n <= 2 ? n : fib2(n - 1) + fib2(n - 2);
}

// Вывод односвязного списка
let list = {
  value: 1,
  next: {
    value: 2,
    next: {
      value: 3,
      next: {
        value: 4,
        next: null,
      },
    },
  },
};

// без рекурсии
function printList(list) {
  let tmp = list;

  while (tmp) {
    console.log(tmp.value);
    tmp = tmp.next;
  }
}

printList(list);

// с рекурсией
function printList2(list) {
  console.log(list.value);
  if (list.next) {
    printList(list.next);
  }
}

printList2(list);

// Вывод односвязного списка в обратном порядке
// с циклом
function printListRevers(list) {
  let tmp = list;
  let arr = [];
  while (tmp) {
    arr.push(tmp.value);
    tmp = tmp.next;
  }
  for (let i = arr.length - 1; i >= 0; i--) {
    console.log(arr[i]);
  }
}

printListRevers(list);
// с рекурсией
function printListRevers2(list) {
  if (list.next) {
    printListRevers2(list.next);
  }
  console.log(list.value);
}

printListRevers2(list);