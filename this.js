'use strict';

function makeUser() {
  return {
    name: 'Джон',
    ref: this,
  };
}

let user = makeUser();
console.log(user.ref.name); // Error: Cannot read property 'name' of undefined

function makeUser2() {
  return {
    name: 'Джон',
    ref() {
      return this;
    },
  };
}

let user2 = makeUser2();

alert(user2.ref().name); // Джон